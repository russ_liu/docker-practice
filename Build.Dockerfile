FROM node:alpine

WORKDIR /source

RUN apk add yarn

COPY ./typeWeb/ /source/

RUN cd /source \
    && yarn \
    && yarn build \
    && rm -rf tsconfig.json src/

WORKDIR /
