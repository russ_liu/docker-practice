import {Request, Response} from "express";

class EchoController {
    echo(req: Request, res: Response) {
        console.log(req.ip);
        res.end(`${req.ip} echo`);
    }
}

export default EchoController;