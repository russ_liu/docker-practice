
import express from 'express';
import EchoRoute from './routes/echoRoute';
import Route from './routes/route';

const routers: Array<Route> = [new EchoRoute()];

const app: express.Application = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

class App {
    private core: express.Application;

    constructor() {
        this.core = express();
        this.config();
        this.views();
        this.routerSetup();
    }

    public getCore() {
        return this.core;
    }

    private config(): void {
        this.core.use(express.json());
        this.core.use(express.urlencoded({ extended: false }));
    }

    private views(): void {
        // this.core.set('views', path.join(__dirname, '../views'));
        // this.core.set('view engine', 'ejs');
    }

    private routerSetup() {
        for (const route of routers) {
            this.core.use(route.getPath(), route.getRoute());
        }
    }
}

export default new App().getCore();