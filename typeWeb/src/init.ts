import app from './app'
const PORT = 1313

app.listen(PORT, ()=>{
    console.log('Starting listening on Port: ', PORT);
});