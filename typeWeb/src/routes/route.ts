import { Router } from "express";

abstract class Route {
    protected route = Router();
    private path: string;
    protected abstract setRoutes(): void;
    constructor(path = '/api') {
        this.path = path;
    }

    public getRoute() :Router {
        return this.route;
    }

    public getPath() :string {
        return this.path;
    }
}

export default Route;