import EchoController from "../controllers/echoController";
import Route from "./route";

class EchoRoute extends Route {
    private controller = new EchoController();
    constructor(){
        super();
        this.setRoutes();
    }

    protected setRoutes(): void {
        this.route.route('/echo').get(this.controller.echo)
    }
}

export default EchoRoute;