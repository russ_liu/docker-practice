# docker practice

- Build.Dockerfile: Compile typescript project. And remove useless files.
- Run.Dockerfile: Deoply program.
## Operate Commands
- Build image
    ```bash
    $ docker-compose build --no-cache
    ```
- Run Container
    ```bash
    $ docker-compose up -d
    ```

- Stop Container
    ```bash
    $ docker-compose down --rmi all
    ```