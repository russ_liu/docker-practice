FROM built:latest

WORKDIR /app

COPY --from=built-code /source/ ./

EXPOSE 1313

ENTRYPOINT node ./dist/init.js